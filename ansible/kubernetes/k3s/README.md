# K3s cluster setup

## Table of contents

- [K3s cluster setup](#k3s-cluster-setup)
  - [Table of contents](#table-of-contents)
  - [Prepare the Ansible configuration](#prepare-the-ansible-configuration)
    - [Fill in the IP addresses into the hosts.ini file](#fill-in-the-ip-addresses-into-the-hostsini-file)
    - [Define the Kube-VIP IP Address](#define-the-kube-vip-ip-address)
    - [Fill out all the required variables in the helm.yml file](#fill-out-all-the-required-variables-in-the-helmyml-file)
    - [Set up the .sops.yaml file](#set-up-the-sopsyaml-file)
    - [Encrypt the helm file](#encrypt-the-helm-file)
    - [Run Ansible playbook](#run-ansible-playbook)
    - [Final steps](#final-steps)

## Prepare the Ansible configuration

### Fill in the IP addresses into the hosts.ini file

In the Terraform section, you specified the IP addresses of both master and worker machines. Now you need to specify them in the [hosts.ini](./inventory/dev/hosts.ini) file, so Ansible can connect to them. If you don't remember what your IP addresses are, please refer to your terraform.tfvars in [this folder](../../../proxmox/kubernetes/k3s/environments/dev/terraform.tfvars).

If your Terraform configuration looks like this:

```bash
# Master Nodes
num_of_k3s_masters = 1
k3s_master_pve_node = ["pve"]
k3s_master_ip_addresses = ["192.168.0.201/24"]

# Worker Nodes
num_of_k3s_workers = 2
k3s_worker_pve_node = ["pve", "pve"]
k3s_worker_ip_addresses = ["192.168.0.204/24", "192.168.0.205/24"]

k3s_gateway = "192.168.0.1"
```

Your hosts.ini file should look like this:

```bash
[master]
192.168.0.201


[node]
192.168.0.204
192.168.0.205

[k3s_cluster:children]
master
node
```

### Define the Kube-VIP IP Address

In addition to the master and worker node IP addresses, you need to define one other IP address to be used as the Kube-VIP IP address. This IP address will be used for the high availability of the control plane.

The Kube-VIP IP address should be in the same range as your other IP addresses and should not be currently used by any other device. Using the IP range from the example above(range 192.168.0.0/24), you can choose an IP address such as 192.168.0.100 unless you know something else is already using it. Write down this Kube-VIP IP address somewhere, as you will need to replace the placeholder KUBE-VIP-IP-ADDRESS with this IP address in the next step.

### Fill out all the required variables in the helm.yml file

The last thing we need to do is to fill out all the required values for the tools like Nginx Ingress Controller, Cert Manager, etc. Some of these tools are only optional, so if you don't want them, just set the enabled field of the specific tool to false. You should already have all the values needed, by following the Prerequisites part of this [README](../../../docs/K3S-PROXMOX.md#prerequisites). Some fields are already filled in, change these only if you have a good reason to do so. Once you fill it out, create a file called helm.yml file in the [inventory/dev/group_vars](./inventory/dev/group_vars/helm.yml) folder, and copy all the variables into it.

```bash
# Kube VIP IP Address
apiserver_endpoint: <KUBE-VIP-IP-ADDRESS> # ex. 192.168.0.100

# K3s Token
k3s_token: <K3S-TOKEN> # Should be Randomly generated string that will be used to connect master and worker nodes

# Secret for Zero SSL
zerossl:
  enabled: true # OPTIONAL: Feel free to set this to false if you don't want to install ZeroSSL Cluster issuer
  eab_kid: <ZeroSSL EAB KID> # EAB KID that we generated earlier when setting up ZeroSSL
  eab_hmac_key: <ZeroSSL EAB HMAC Key> # EAB HMAC Key that we generated earlier when setting up ZeroSSL
cert_manager:
  enabled: true # OPTIONAL: Feel free to set this to false if you don't want to install Cert Manager
ingress_nginx:
  enabled: true # OPTIONAL: Feel free to set this to false if you don't want to install Ingress Nginx controller
cloudflare:
  enabled: true # OPTIONAL: Feel free to set this to false if you don't want to install Cloudflare Tunnel
  image:
    tag: 2024.6.0
  account: <CLOUDFLARE-ACCOUNT-TAG> # Account tag that we got from the json file when setting up the Cloudflare Tunnel
  tunnelName: k3s-dev # Leave it as is, or set it to whatever you like
  tunnelId:  <CLOUDFLARE-TUNNEL-ID> # Tunnel ID that we got from the json file when setting up the Cloudflare Tunnel
  secret: <CLOUDFLARE-TUNNEL-SECRET> # Tunnel SECRET that we got from the json file when setting up the Cloudflare Tunnel
  ingress:
    - hostname: '*.<YOUR-DOMAIN>' # ex. *.mydomain.cloud
      service: https://ingress-nginx-controller:443
      originRequest:
          originServerName: <YOUR-DOMAIN>  # ex. mydomain.cloud
          noTLSVerify: true
    - hostname: '<YOUR-DOMAIN>' # ex. mydomain.cloud
      service: https://ingress-nginx-controller:443
      originRequest:
          originServerName: <YOUR-DOMAIN>  # ex. mydomain.cloud
          noTLSVerify: true

# Gitlab runner configuration
gitlab_runner:
  enabled: true # OPTIONAL: Feel free to set this to false if you don't want to install Gitlab Runner
  gitlabUrl: https://gitlab.com
  rbac:
      create: true
      clusterWideAccess: true
      rules:
          apiGroups:
              - ""
              - apps
              - networking.k8s.io
  runners:
      config: |
          [[runners]]
            name="enthusiastix-kubernetes-runner"
            executor = "kubernetes"
            [runners.kubernetes]
              image = "alpine"
              privileged = true
              # Required because of a bug no one fixed for years - https://gitlab.com/gitlab-org/charts/gitlab-runner/-/issues/353
              service_account = "gitlab-runner"
  runnerToken: <RUNNER-TOKEN> # ex. glrt-6zsSdfdnqDXuMNda7Dk7-W
```

### Set up the .sops.yaml file

To be able to use sops in your project, you need to create a .sops.yaml file in the [ansible/kubernetes/k3s](./) folder of your repository, and then specify an encryption key for every file that you want to encrypt. Add the code below to your .sops.yaml file, and replace placeholders with the key that you generated earlier. If you don't know what the key vault value should be, please refer to the Azure part of the Prerequisites section in [this README file.](../../../docs/K3S-PROXMOX.md#set-up-azure-resources-needed-for-this-project#Azure-Variables)

```bash
creation_rules:
  - path_regex: helm* # This will allow us to encrypt our helm file
    azure_keyvault: <AZURE-KEYVAULT-KEY>
```

### Encrypt the helm file

Before we start setting up the cluster using Ansible, we have to encrypt the file with all the values that will be stored in Git. We also need to run the az login command first, so we can access Azure keyvault. Once you encrypt the file, commit all the changes into Git, and move to the jump host, from where we will once again pull all the latest changes that we just pushed, decrypt the helm file, and then run the Ansible commands.

```bash
az login
sops -e ./inventory/dev/group_vars/helm.yml > ./inventory/dev/group_vars/helm.enc.yml
```

### Run Ansible playbook

The last thing that is left to do is to run the Ansible playbook with the command below but first, I would like to mention two potential problems that might occur.

The first problem is that you might get this error: ```AnsibleFilterError: Failed to import the required Python library (netaddr) on...``` \
To fix this, please run this command ```sudo apt install python3-netaddr -y```

The second problem is that after running the Ansible command, sometimes what happened was that it got stuck on the ```TASK [k3s_agent : Enable and check K3s service```.

This step might take a minute or two, but if it gets stuck for longer than that, you need to stop the Ansible command and run it again, which always fixed the problem for me.

With that out of the way, let's decrypt our helm.enc.yml file and run the Ansible playbook, using the commands below:

```bash
az login
sops -d ./inventory/dev/group_vars/helm.enc.yml > ./inventory/dev/group_vars/helm.yml
ansible-playbook -i inventory/dev/hosts.ini site.yml -e @inventory/dev/group_vars/general.yml -e @inventory/dev/group_vars/helm.yml
```

### Final steps

[Let's go back to the first README to finish the final steps.](../../../docs/K3S-PROXMOX.md#step-3-retrieve-the-kubecontext-and-test-the-connection-to-the-cluster)
