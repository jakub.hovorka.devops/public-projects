# VMs for Multi-Node K3s cluster

## Table of contents

- [VMs for Multi-Node K3s cluster](#vms-for-multi-node-k3s-cluster)
  - [Table of contents](#table-of-contents)
  - [Project description](#project-description)
  - [Setup guide](#setup-guide)
    - [Create a Proxmox VM template for our K3s nodes](#create-a-proxmox-vm-template-for-our-k3s-nodes)
    - [Set up Terraform role and user](#set-up-terraform-role-and-user)
    - [Generate the Proxmox API token](#generate-the-proxmox-api-token)
    - [Create a terraform.tfvars file and fill in the Proxmox credentials](#create-a-terraformtfvars-file-and-fill-in-the-proxmox-credentials)
    - [Fill in the SSH configuration into the terraform.tfvars file](#fill-in-the-ssh-configuration-into-the-terraformtfvars-file)
    - [Fill in the rest of the configuration into the terraform.tfvars file](#fill-in-the-rest-of-the-configuration-into-the-terraformtfvars-file)
    - [Fill in the remote backend configuration into the providers.tf file](#fill-in-the-remote-backend-configuration-into-the-providerstf-file)
    - [Set up the .sops.yaml file](#set-up-the-sopsyaml-file)
    - [Encrypt the tfvars file and the certificate files](#encrypt-the-tfvars-file-and-the-certificate-files)
    - [Deploying VMs with Terraform](#deploying-vms-with-terraform)
    - [Copy the certificates to the .ssh folder on the jump host](#copy-the-certificates-to-the-ssh-folder-on-the-jump-host)
    - [Install K3s and other tools on the VMs using Ansible](#install-k3s-and-other-tools-on-the-vms-using-ansible)

## Project description

The purpose of this project is to deploy virtual machines that will serve as master and worker nodes for a K3s cluster.
Currently, there is only the Proxmox module, but we could also add other modules that would use cloud providers like Digital Ocean, or Azure.

## Setup guide

### Create a Proxmox VM template for our K3s nodes

The Proxmox provider creates the VMs based on the VM Template, so we have to go to the Proxmox GUI, enter the shell of our Proxmox server, and run all the commands below to create the VM template.
If you just installed Proxmox, using the default values, and haven't done any customizations, these commands should work for you as is but if you customized your Proxmox server, you may need to adjust some things. Please leave the template name set to k3s-template because if you change it, the Terraform configuration would break, because the name is specified in the variables.tf file.

```bash
# Install libguestfs-tools
apt install libguestfs-tools -y

# Download Debian 12 generic AMD64 cloud image
wget https://cloud.debian.org/images/cloud/bookworm/latest/debian-12-generic-amd64.qcow2

# Install QEMU guest agent in the cloud image
virt-customize -a debian-12-generic-amd64.qcow2 --install qemu-guest-agent

# Enable QEMU guest agent service
virt-customize -a debian-12-generic-amd64.qcow2 --run-command 'systemctl enable qemu-guest-agent'

# Clear machine-id to avoid conflicts in cloned VMs
virt-customize -a debian-12-generic-amd64.qcow2 --run-command "echo -n > /etc/machine-id"

# Create an empty configuration file for the new VM
touch /etc/pve/nodes/pve/qemu-server/9992.conf

# Create a new VM (ID: 9992) with 4GB of memory and virtio network interface
qm create 9992 --memory 4096 --net0 virtio,bridge=vmbr0

# Import the Debian 12 cloud image into the local-lvm storage pool
qm importdisk 9992 debian-12-generic-amd64.qcow2 local-lvm --format qcow2

# Set the SCSI controller and disk for the VM
qm set 9992 --scsihw virtio-scsi-pci --scsi0 local-lvm:vm-9992-disk-0

# Enable QEMU agent and set the VM name
qm set 9992 --agent enabled=1,fstrim_cloned_disks=1
qm set 9992 --name k3s-template

# Set the cloud-init configuration to the second IDE interface
qm set 9992 --ide2 local-lvm:cloudinit

# Set the boot order to boot from the SCSI disk
qm set 9992 --boot c --bootdisk scsi0

# Configure serial and VGA settings
qm set 9992 --serial0 socket --vga serial0

# Resize the disk by adding 8GB to it
qm resize 9992 scsi0 +8G

# Convert the VM to a template
qm template 9992
```

### Set up Terraform role and user

Terraform will deploy the VMs using the Proxmox API, and it requires an API token to do that. We will first need to create a user that will be used to generate the API token, so we have to go to the Proxmox GUI, enter the shell of our Proxmox server, and run all the commands below(don't forget to fill in the password instead of the placeholder) to create the Proxmox role, the user, and then assign the role that we just created to the user.

```bash
pveum role add TerraformProv -privs "Datastore.AllocateSpace Datastore.Audit Pool.Allocate Sys.Audit Sys.Console Sys.Modify VM.Allocate VM.Audit VM.Clone VM.Config.CDROM VM.Config.Cloudinit VM.Config.CPU VM.Config.Disk VM.Config.HWType VM.Config.Memory VM.Config.Network VM.Config.Options VM.Migrate VM.Monitor VM.PowerMgmt SDN.Use"
pveum user add terraform-prov@pve --password <password>
pveum aclmod / -user terraform-prov@pve -role TerraformProv
```

### Generate the Proxmox API token

To generate the Proxmox API token, go to the Proxmox GUI, click on Datacenter, and then you should see the API Tokens option under the Permissions dropdown. Select this option and click the Add button. The user should be the user called "terraform-prov@pve" that we just created, Token ID can be set to "terraform", and Privilege Separation should NOT be checked. After generating the token, save the token secret, and token id.

### Create a terraform.tfvars file and fill in the Proxmox credentials

Go to the [environments/dev](./environments/dev) folder, create a terraform.tfvars file, and add the code below(replace the placeholders with the Token ID, the Token Secret, and the IP address of your Proxmox server - this address must be accessible from the machine where we will run the Terraform commands - which in my case, is the LXC container on the Proxmox server, that I use as a jump host).

```bash
pve_token_id = "<your-token-id>" # Ex. terraform-prov@pve!terraform
pve_token_secret = "<your-token-secret>" # Ex. 1944a765-85d8-43c7-ab44-b7e5aa392761
pve_api_url = "https://<ip-address-of-your-proxmox-server>:8006/api2/json"
```

### Fill in the SSH configuration into the terraform.tfvars file

For the user, I recommend just using root, because in the future, if you decide to combine VMs from multiple sources,
Terraform providers of some cloud platforms sometimes don't have a dedicated configuration field to add a new user. I had this issue with Digital Ocean, where I had to add a custom init script that would handle the user creation, which is not the most difficult thing in the world but it adds more complexity to your setup, for very little benefit. After that, generate a secure password, and also generate an SSH key, using the "ssh-keygen -t rsa -b 4096 -f ~/id_rsa" command. After you generate the SSH keys, move them to the current folder, and specify the path to them, in the terraform.tfvars file.

```bash
ssh_user = "root" # This is the user that will be created in your VMs
ssh_password = "<fill-in-your-password>" # The password of the root user that will be used to log into these VMs
ssh_key_public = "./id_rsa.pub"
ssh_key_private = "./id_rsa"
```

### Fill in the rest of the configuration into the terraform.tfvars file

You need to specify the gateway, the number of master and worker nodes, and assign node IP Addresses based on the IP address of your Proxmox Server. For example, if the IP address range of your server is 192.168.0.0/24  For each node IP address, you also need to add the name of the Proxmox Server node on which should the VM be created, to the pve_node list variable. In my case, I only have one node, and I haven't changed its name, so I just add "pve" (which is the default name) for every IP Address. Please write this down as you will need to specify individual IP addresses of all master and worker nodes, later in the Ansible part of this guide.

```bash
# Master Nodes
num_of_k3s_masters = 1
k3s_master_pve_node = ["pve"]
k3s_master_ip_addresses = ["192.168.0.201/24"]

# Worker Nodes
num_of_k3s_workers = 2
k3s_worker_pve_node = ["pve", "pve"]
k3s_worker_ip_addresses = ["192.168.0.204/24", "192.168.0.205/24"]

k3s_gateway = "192.168.0.1"
```

You also need to update the resource configuration of the VMs based on your needs.

```bash
# Master Nodes
k3s_master_memory = "5096"
k3s_master_cpu_cores = "1"
k3s_master_root_disk_size = 16
k3s_master_data_disk_size = 32

# Worker Nodes
k3s_worker_memory = "5096"
k3s_worker_cpu_cores = "1"
k3s_worker_root_disk_size = 16
k3s_worker_data_disk_size = 32
```

### Fill in the remote backend configuration into the providers.tf file

To use the Azure storage container to store our state file, we need to fill in a few details like our container name, storage account name, resource group in which the storage account resides, and also the subscription under which was the resource group created. This needs to be filled in to the providers.tf file, in the environments/dev folder. You can find placeholders in there, if you don't know what some of the values should be, please refer to the Azure part of the Prerequisites section in [this README file.](../../../docs/K3S-PROXMOX.md#set-up-azure-resources-needed-for-this-project#Azure-Variables)

### Set up the .sops.yaml file

To be able to use sops in your project, you need to create a .sops.yaml file in the [terraform/proxmox/kubernetes/k3s](./) folder of your repository, and then specify an encryption key for every file that you want to encrypt. Add the code below into your .sops.yaml file, and replace placeholders with the key that you generated earlier. If you don't know what the key vault value should be, please refer to the Azure part of the Prerequisites section in [this README file.](../../../docs/K3S-PROXMOX.md#set-up-azure-resources-needed-for-this-project#Azure-Variables)

```bash
creation_rules:
  - path_regex: terraform.tfvars* # This will allow us to encrypt our tfvars file
    azure_keyvault: <AZURE-KEYVAULT-KEY>
  - path_regex: id_rsa.* # This will allow us to encrypt our certificates
    azure_keyvault: <AZURE-KEYVAULT-KEY>
```

### Encrypt the tfvars file and the certificate files

Before we start deploying VMs, we have to do one last thing, which is encrypting the files that will be stored in Git, like the terraform.tfvars file, and also the certificates. We also need to run the az login command first, so we can access Azure keyvault. If you don't have AZ CLI locally, please install it using [this](https://learn.microsoft.com/en-us/cli/azure/install-azure-cli) guide.

```bash
az login
sops -e terraform.tfvars > terraform.tfvars.enc
sops -e id_rsa > id_rsa.enc
sops -e id_rsa.pub > id_rsa.pub.enc
```

### Deploying VMs with Terraform

Now we can finally deploy the VMs. We will deploy it through the jump host, so commit all the updates to your git repository, then clone it to the jump host, and decrypt all the encrypted files. After that, you can run the Terraform commands, and you should be done.

```bash
az login
sops -d terraform.tfvars.enc > terraform.tfvars
sops -d id_rsa.enc > id_rsa
sops -d id_rsa.pub.enc > id_rsa.pub
terraform init
terraform apply -var-file terraform.tfvars
```

### Copy the certificates to the .ssh folder on the jump host

The last thing we need to do is to add the SSH certificates to the jump host, so we can then use Ansible to set up the VMs that we deployed with Terraform. Without this, we would get an Access denied error, when running the Ansible commands in the next step. We already decrypted the certificates in the previous step, so you just need to go to the folder with the certificates, and run the command below:

```bash
# Copy the certificates
cp id_rsa id_rsa.pub ~/.ssh && chmod 600 ~/.ssh/id_rsa  ~/.ssh/id_rsa.pub
```

When copying the certificate, sometimes, there is an extra empty line at the end of the cert file, which will then cause an error like this: ```Error loading key "/home/tailscale/.ssh/id_rsa": error in libcrypto```
To fix it, you will need to run ```nano ~/.ssh/id_rsa``` and delete the empty line at the end of the file.
After doing that, you also need to start the SSH agent and load all the SSH keys into it. You can do it by running the 2 commands below:

```bash
eval "$(ssh-agent -s)"
ssh-add
```

### Install K3s and other tools on the VMs using Ansible

[Let's go to the Ansible section of this repository](../../../../ansible/kubernetes/k3s/README.md)
