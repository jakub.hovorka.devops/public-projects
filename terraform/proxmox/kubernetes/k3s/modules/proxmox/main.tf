####################### K3S MASTER VMs #######################
resource "proxmox_vm_qemu" "proxmox_vm_master" {
  count       = var.num_of_k3s_masters
  name        = "k3s-master-${count.index}"
  desc        = "K3S Master Node"
  ipconfig0   = "gw=${var.k3s_gateway},ip=${var.k3s_master_ip_addresses[count.index]}"
  target_node = var.k3s_master_pve_node[count.index]
  onboot      = true
  cloudinit_cdrom_storage = "local-lvm"
  hastate     = "started"
  # Same CPU as the Physical host, possible to add cpu flags
  # Ex: "host,flags=+md-clear;+pcid;+spec-ctrl;+ssbd;+pdpe1gb"
  cpu        = "host"
  numa       = false
  clone      = var.vm_template_name
  os_type    = "cloud-init"
  agent      = 1
  memory     = var.k3s_master_memory
  cores      = var.k3s_master_cpu_cores
  nameserver = var.k3s_nameserver
  sshkeys    = file(var.ssh_key_public)
  ciuser     = var.ssh_user
  cipassword = var.ssh_password

  network {
    model  = "virtio"
    bridge = "vmbr0"
    tag    = var.k3s_vlan
  }

  serial {
    id   = 0
    type = "socket"
  }

  vga {
    type = "serial0"
  }

  disks {
    scsi {
      scsi0 {
        disk {
          size    = var.k3s_master_root_disk_size
          storage = var.k3s_master_disk_storage
          backup  = false
        }
      }
      scsi1 {
        disk {
          size    = var.k3s_master_data_disk_size
          storage = var.k3s_master_disk_storage
          backup  = false
        }
      }
    }
  }

  connection {
    type        = "ssh"
    user        = var.ssh_user
    private_key = file(var.ssh_key_private)
    host        = self.ssh_host
  }

  lifecycle {
    ignore_changes = [
      network, disk, sshkeys, target_node
    ]
  }
}
##############################################

####################### K3S WORKER VMs #######################
resource "proxmox_vm_qemu" "proxmox_vm_workers" {
  count       = var.num_of_k3s_workers
  name        = "k3s-worker-${count.index}"
  ipconfig0   = "gw=${var.k3s_gateway},ip=${var.k3s_worker_ip_addresses[count.index]}"
  target_node = var.k3s_worker_pve_node[count.index]
  onboot      = true
  cloudinit_cdrom_storage = "local-lvm"
  hastate     = "started"
  # Same CPU as the Physical host, possible to add cpu flags
  # Ex: "host,flags=+md-clear;+pcid;+spec-ctrl;+ssbd;+pdpe1gb"
  cpu        = "host"
  numa       = false
  clone      = var.vm_template_name
  os_type    = "cloud-init"
  agent      = 1
  memory     = var.k3s_worker_memory
  cores      = var.k3s_worker_cpu_cores
  nameserver = var.k3s_nameserver
  sshkeys    = file(var.ssh_key_public)
  ciuser     = var.ssh_user
  cipassword = var.ssh_password

  network {
    model  = "virtio"
    bridge = "vmbr0"
    tag    = var.k3s_vlan
  }

  serial {
    id   = 0
    type = "socket"
  }

  vga {
    type = "serial0"
  }

  disks {
    scsi {
      scsi0 {
        disk {
          size    = var.k3s_worker_root_disk_size
          storage = var.k3s_worker_disk_storage
          backup  = false 
        }
      }
      scsi1 {
        disk {
          size    = var.k3s_worker_data_disk_size
          storage = var.k3s_worker_disk_storage
          backup  = false
        }
      }
    }
  }

  connection {
    type        = "ssh"
    user        = var.ssh_user
    private_key = file(var.ssh_key_private)
    host        = self.ssh_host
  }

  lifecycle {
    ignore_changes = [
      network, disk, sshkeys, target_node
    ]
  }
}
##############################################