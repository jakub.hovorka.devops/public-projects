####################### Proxmox #######################
variable "pve_api_url" {
  description = "The URL to the Proxmox API"
  type        = string
  sensitive   = false
  default     = "https://localhost:8006/api2/json"
}

variable "pve_token_id" {
  description = "Proxmox API token"
  type        = string
  sensitive   = false
}

variable "pve_token_secret" {
  description = "Proxmox API secret"
  type        = string
  sensitive   = false
}
##############################################

####################### K3s Common #######################
variable "k3s_gateway" {
  type    = string
  default = "192.168.0.1"
}

variable "k3s_vlan" {
  default = -1
}

variable "vm_template_name" {
  default = "k3s-template"
}

variable "k3s_nameserver" {
  type    = string
  default = "8.8.8.8"
}

variable "ssh_user" {
  description = "Username for ssh to nodes"
  type        = string
  default     = "root"
}

variable "ssh_password" {
  description = "SSH Password (Optional)"
  type        = string
  default     = null
}

variable "ssh_key_private" {
  description = "SSH private key"
  type        = string
}

variable "ssh_key_public" {
  description = "SSH public key"
  type        = string
}
##############################################

####################### K3s Master Nodes #######################
variable "num_of_k3s_masters" {
  default = 1
}

variable "k3s_master_pve_node" {
  description = "The PVE node to target"
  type        = list(string)
  sensitive   = false
  default     = ["pve"]
}

variable "k3s_master_ip_addresses" {
  description = "List of IP addresses for master node(s)"
  type        = list(string)
  default     = ["192.168.0.201/24"]
}

variable "k3s_master_memory" {
  default = "5096"
}

variable "k3s_master_cpu_cores" {
  default = "1"
}

variable "k3s_master_root_disk_size" {
  default = 16
}

variable "k3s_master_data_disk_size" {
  default = 32
}

variable "k3s_master_disk_storage" {
  default = "local-lvm"
}
##############################################

####################### K3s Worker Nodes #######################
variable "num_of_k3s_workers" {
  default = 2
}

variable "k3s_worker_pve_node" {
  description = "The PVE node to target"
  type        = list(string)
  sensitive   = false
  default     = ["pve", "pve"]
}

variable "k3s_worker_ip_addresses" {
  description = "List of IP addresses for master node(s)"
  type        = list(string)
  default     = ["192.168.0.204/24", "192.168.0.205/24"]
}

variable "k3s_worker_memory" {
  default = "5096"
}

variable "k3s_worker_cpu_cores" {
  default = "1"
}

variable "k3s_worker_root_disk_size" {
  default = 16
}

variable "k3s_worker_data_disk_size" {
  default = 32
}

variable "k3s_worker_disk_storage" {
  default = "local-lvm"
}
##############################################