#!/bin/bash

if [ $# -ne 1 ]; then
    echo "Usage: $0 <encrypt/decrypt>"
    exit 1
fi

operation=$1

if [ "$operation" == "encrypt" ]; then
    sops -e id_rsa > id_rsa.enc
    sops -e id_rsa.pub > id_rsa.pub.enc
    sops -e terraform.tfvars > terraform.tfvars.enc
    echo "Encryption complete."
elif [ "$operation" == "decrypt" ]; then
    sops -d id_rsa.enc > id_rsa
    sops -d id_rsa.pub.enc > id_rsa.pub
    sops -d terraform.tfvars.enc > terraform.tfvars
    echo "Decryption complete."
else
    echo "Invalid operation. Use 'encrypt' or 'decrypt'."
    exit 1
fi
