module "proxmox" {
  source          = "../../modules/proxmox"
  # Proxmox
  pve_token_id     = var.pve_token_id
  pve_token_secret  = var.pve_token_secret
  pve_api_url  = var.pve_api_url
  # Proxmox VM SSH configuration
  ssh_key_public     = var.ssh_key_public
  ssh_key_private  = var.ssh_key_private
  ssh_user  = var.ssh_user
  ssh_password = var.ssh_password
  # Proxmox VMs configuration
  # Common
  k3s_gateway = var.k3s_gateway
  # Master Nodes
  num_of_k3s_masters = var.num_of_k3s_masters
  k3s_master_pve_node = var.k3s_master_pve_node
  k3s_master_ip_addresses = var.k3s_master_ip_addresses
  # Worker Nodes
  num_of_k3s_workers = var.num_of_k3s_workers
  k3s_worker_pve_node = var.k3s_worker_pve_node
  k3s_worker_ip_addresses = var.k3s_worker_ip_addresses
}
