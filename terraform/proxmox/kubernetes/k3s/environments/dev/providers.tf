terraform {
  backend "azurerm" {
    subscription_id      = "<AZURE-SUBSCRIPTION-ID>"
    resource_group_name  = "<AZURE-RESOURCE-GROUP>"
    storage_account_name = "<AZURE-STORAGE-ACCOUNT-NAME>"
    container_name       = "<AZURE-STORAGE-ACCOUNT-CONTAINER>"
    key                  = "terraform.tfstate" # Name that will be used to store the terraform state file in the AZ container,
                                               # feel free to keep as is.
  }

  required_providers {
    proxmox = {
      source = "Telmate/proxmox"
      version = "3.0.1-rc1"
    }
  }
}

provider "proxmox" {
  pm_api_url          = var.pve_api_url
  pm_api_token_id     = var.pve_token_id
  pm_api_token_secret = var.pve_token_secret
  pm_log_enable       = false
  pm_log_file         = "terraform-plugin-proxmox.log"
  pm_parallel         = 1
  pm_timeout          = 600
  pm_log_levels = {
    _default    = "debug"
    _capturelog = ""
  }
}
