# K3s Kubernetes Cluster on Proxmox

## Table of Contents

- [K3s Kubernetes Cluster on Proxmox](#k3s-kubernetes-cluster-on-proxmox)
  - [Table of Contents](#table-of-contents)
  - [Project Description](#project-description)
    - [Ingress Nginx Controller and Cert Manager](#ingress-nginx-controller-and-cert-manager)
    - [ZeroSSL](#zerossl)
    - [Gitlab Runner](#gitlab-runner)
    - [Cloudflare Tunnel](#cloudflare-tunnel)
  - [Preface](#preface)
    - [Differences from Enterprise Setups](#differences-from-enterprise-setups)
    - [Creation of this Guide](#creation-of-this-guide)
  - [Before you begin](#before-you-begin)
  - [Prerequisites](#prerequisites)
    - [Create a GitLab Account if you don't have one yet](#create-a-gitlab-account-if-you-dont-have-one-yet)
    - [Fork this repository](#fork-this-repository)
    - [Getting a server](#getting-a-server)
    - [Set up the Proxmox server](#set-up-the-proxmox-server)
    - [Setup a LXC Template that will be used for Tailscale](#setup-a-lxc-template-that-will-be-used-for-tailscale)
    - [Set up a Linux container that will work as a jump host and set up Tailscale for remote access possibility](#set-up-a-linux-container-that-will-work-as-a-jump-host-and-set-up-tailscale-for-remote-access-possibility)
    - [Accessing the jump host machine externally using Tailscale](#accessing-the-jump-host-machine-externally-using-tailscale)
    - [Configure and install dependencies on the Linux container jump host](#configure-and-install-dependencies-on-the-linux-container-jump-host)
    - [Generate an SSH key for Gitlab on your jump host](#generate-an-ssh-key-for-gitlab-on-your-jump-host)
    - [Set up Azure resources needed for this project](#set-up-azure-resources-needed-for-this-project)
      - [Azure Variables](#azure-variables)
    - [Buy a domain (OPTIONAL: Only if you want to set up an ingress controller and a cert-manager)](#buy-a-domain-optional-only-if-you-want-to-set-up-an-ingress-controller-and-a-cert-manager)
    - [Set up a Cloudflare tunnel (OPTIONAL: Only if you want to expose your cluster without exposing your network)](#set-up-a-cloudflare-tunnel-optional-only-if-you-want-to-expose-your-cluster-without-exposing-your-network)
    - [Set up a ZeroSSL account (OPTIONAL: Only if you want to have valid certificates when you expose your services)](#set-up-a-zerossl-account-optional-only-if-you-want-to-have-valid-certificates-when-you-expose-your-services)
    - [Set up a Gitlab runner (OPTIONAL: Only if you want to use Gitlab as your CI/CD solution.)](#set-up-a-gitlab-runner-optional-only-if-you-want-to-use-gitlab-as-your-cicd-solution)
    - [Configure SOPS for secret file encryption](#configure-sops-for-secret-file-encryption)
  - [STEP 1: Terraform](#step-1-terraform)
    - [Let's move to the Terraform part of this guide](#lets-move-to-the-terraform-part-of-this-guide)
  - [STEP 2: Ansible](#step-2-ansible)
    - [Let's move to the Ansible part of this guide](#lets-move-to-the-ansible-part-of-this-guide)
  - [STEP 3: Retrieve the Kubecontext, and test the connection to the cluster](#step-3-retrieve-the-kubecontext-and-test-the-connection-to-the-cluster)
    - [Retrieve kubecontext and create an SSH tunnel to the master node's port 6443 to access the K8s cluster](#retrieve-kubecontext-and-create-an-ssh-tunnel-to-the-master-nodes-port-6443-to-access-the-k8s-cluster)
  - [Troubleshooting](#troubleshooting)

## Project Description

In this project, you're going to set up a Proxmox server on a bare metal server, deploy VMs using Terraform, and set up a lightweight K3s Kubernetes cluster, together with multiple useful tools, using Ansible. The tools that are installed on the K8s cluster by Ansible are:

- **Ingress Nginx Controller**
- **Cert Manager**
- **ZeroSSL Cluster Issuer**
- **Gitlab runner**
- **Cloudflare Tunnel**

Mind that installation of any of these tools can be disabled if you don't want to use them, or want to use some alternative. Let's go over these tools a bit:

### Ingress Nginx Controller and Cert Manager

Both the Ingress Nginx Controller and Cert Manager are very popular and standard tools, that are a must-have if you want to expose apps running in your K8s cluster to the public internet.

### ZeroSSL

ZeroSSL is a certificate authority used as a cluster issuer in Kubernetes, providing free SSL certificates for securing communication within the cluster. Typically, most people use Let's Encrypt as their cluster issuer, which is what I originally wanted to use, but for some reason, Let's Encrypt just did not work for me on K3s in Proxmox. I was stuck on some TLS-related error, and I haven't found any solution even after spending 2 evenings debugging and trying all kinds of fixes I found online. Because of that, I decided to use ZeroSSL, which is free and worked right away.

### Gitlab Runner

Next, we have a Gitlab runner, which I chose simply because I plan to deploy all of my applications using Gitlab CI/CD pipelines. By deploying the runner on your cluster, you can deploy apps using pipelines right away.

### Cloudflare Tunnel

The last tool is the Cloudflare Tunnel, and I decided to use it because it allows me to securely expose my applications to the internet with minimal setup, and at the same time, it does not expose my whole server. The Cloudflare tunnel instance running in the cluster establishes a secure connection with Cloudflare's network, which then allows traffic to be redirected from my domain to Cloudflare's network, and then to the Cloudflare tunnel in my cluster. Once the traffic reaches the Cloudflare tunnel, it is further redirected to the ingress controller, from where it's routed to the applications.

## Preface

I started this project because I work as a DevOps Engineer, and I wanted to have a home lab with a tech stack similar to what a lot of companies use nowadays, which is a Kubernetes cluster with tools like the Ingress Nginx Controller, and the Cert-manager, set up using IaaC tools like Terraform and Ansible, where you can deploy applications using GitLab CI/CD or other tools like ArgoCD/Jenkins. The main motivation was to have a place where I could experiment with interesting tools, and continuously build a library of resources that might be useful in my career.

### Differences from Enterprise Setups

This setup is different from the one that enterprises would use as most of the companies that run everything on Kubernetes prefer to either use managed Kubernetes clusters like AKS, EKS, or GKE, or OpenStack if they choose to go the on-premise way. However, having a managed Kubernetes cluster as a home lab would be very expensive, and by setting up and managing the Kubernetes cluster yourself, you can learn a lot of things that are normally abstracted away, which is why I decided to buy an affordable mini PC and make it into a server, using a Proxmox hypervisor. I chose Proxmox because it seemed like the easiest way to go but in the future, I plan to try out OpenStack as well.

### Creation of this Guide

The reason I decided to create this guide is because while searching for guides that I could use for this project, I couldn't find anything that would cover everything I wanted to do, so I decided I would piece it all together, using some of the existing guides and my knowledge. This turned out to be a very difficult task because sometimes I found a guide that did exactly what I needed but some parts were either outdated, or done in a bit messy way, so I had to spend a lot of time debugging, and refactoring to make it work together with what I already had.

In the guide, there are some parts that I cover by sharing a link to another guide, because for common things like setting up a Proxmox server, creating a Gitlab or Azure account, installing tools, etc., there are already tons of existing guides, so it wouldn't make sense for me to cover that as well. However, besides the most basic things, this guide covers everything that you're going to need, and it was already tested from start to finish.

## Before you begin

When progressing through the guide, please always read all the text first, before proceeding to the links because sometimes, I mention some issues that you might encounter while using the resources in the links, so it might save you a lot of time debugging.

## Prerequisites

### Create a GitLab Account if you don't have one yet

You can find how to do it [here](https://gitlab.com/users/sign_up)

### Fork this repository

You can find how to do it [here](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html#create-a-fork)

### Getting a server

I would recommend a dedicated machine with at least 4 physical cores and 16GB of RAM. I bought [this exact mini PC](https://amzn.eu/d/6TybGZv) for about 190€, but you can find the same PC under many different names, so I would just look for the cheapest one that you can find, they are very often on sale. The ideal specs of these Mini PCs should be Intel N95/N100 CPU, 16GB RAM, and 256GB/512GB SSD.

### Set up the Proxmox server

To start, we need to first set up Proxmox on our server. All we need is a fresh installation with the default configuration.
There are a lot of different resources that guide you on how to do it, so I will give you a link to one but there is a one pro tip I would like to give you first, which is that always have your server plugged to your router with an internet cable before you start installing Proxmox, because If you won't do that, it will not fill out your network configuration automatically. This might be obvious to you but I did not know that, so just wanted to mention it. As for the guide to use, this is a really nice one: \
<https://www.youtube.com/watch?v=5axVd19Jris>

### Setup a LXC Template that will be used for Tailscale

Before getting any further, we need to download a Linux container template that will be used in the next step. We will be using Ubuntu 22.04. To download it, go to your Proxmox GUI, then click Datacenter, pve, local (pve), choose CT Templates, and then click Download from URL, input this link:
<http://download.proxmox.com/images/system/ubuntu-22.04-standard_22.04-1_amd64.tar.zst> ,
and name it whatever you like.

### Set up a Linux container that will work as a jump host and set up Tailscale for remote access possibility

I chose to create a Linux Container jump host and set up a Tailscale VPN because my server needs to be
accessed from the outside of my network and I cannot do port-forwarding because the router I got from my ISP does not allow it. Tailscale offers a safe way to expose your network and grant access to others. It also has a lot of other features, and everything we need is included in its free tier.

Setting up a Linux container with Tailscale is straightforward and there is an excellent step-by-step guide for that:

<https://youtu.be/QJzjJozAYJo?si=nrNRdAk845iWDd1e>

### Accessing the jump host machine externally using Tailscale

Now that you have the jump host machine set up with Tailscale installed, you can leave the Proxmox server machine as is, and switch to your laptop/desktop.

To be able to connect to your Proxmox server from your laptop/desktop, you first need to install the Tailscale client - available on [Windows](https://tailscale.com/kb/1022/install-windows), [MacOS](https://tailscale.com/kb/1016/install-mac), or [Linux](https://tailscale.com/kb/1031/install-linux).

Then run the client, and log into your Tailscale account.

After that, go to the [Tailscale Admin GUI](https://login.tailscale.com/admin/machines) and find the IP Address of the jump host machine, that was automatically assigned by Tailscale.

Once you know the Tailscale IP Address of the jump host machine, fill it in below, together with the private IP address of the jump host machine, and run all the commands in your terminal:

```bash
# This command creates an SSH tunnel and sets up port forwarding to the Proxmox GUI on your server,
# enabling remote access and command execution.
# PROXMOX_SERVER_PRIVATE_IP: Replace this with the private IP address of your Proxmox server that I mentioned you should write down in the previous step.
# JUMP_HOST_TAILSCALE_IP: Replace this with the IP address that Tailscale assigned to the jump host machine
ssh -L 9881:<PROXMOX_SERVER_PRIVATE_IP>:8006 root@<JUMP_HOST_TAILSCALE_IP>
```

Now you should be able to access your Proxmox GUI on <https://localhost:9881>, and also run commands on the jump host machine. It's good to save this command somewhere because this is how you(and other people you grant access to) are going to be interacting with your Proxmox server from now on. In the next step, we will install dependencies on the jump host machine.

### Configure and install dependencies on the Linux container jump host

First, let's create a user, so we don't access the jump host using the root user. I will call my user tailscale, but you name it whatever you want.

```bash
# Create a user called tailscale
sudo adduser tailscale

# Give it sudo permissions
sudo usermod -aG sudo tailscale

# Log in as that user
su - tailscale

# Test out sudo access
sudo ls /root
```

Now whenever you use the SSH command from the previous step, specify the user as tailscale, instead of root. Now let's install all the tools that we will need for our project on the jump host.

```bash
# Curl and Git
sudo apt-get update && sudo apt-get upgrade -y && sudo apt-get install -y curl git

# Terraform - source: https://developer.hashicorp.com/terraform/tutorials/aws-get-started/install-cli
sudo apt-get update && sudo apt-get install -y gnupg software-properties-common
wget -O- https://apt.releases.hashicorp.com/gpg | \
gpg --dearmor | \
sudo tee /usr/share/keyrings/hashicorp-archive-keyring.gpg > /dev/null
gpg --no-default-keyring \
--keyring /usr/share/keyrings/hashicorp-archive-keyring.gpg \
--fingerprint
echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] \
https://apt.releases.hashicorp.com $(lsb_release -cs) main" | \
sudo tee /etc/apt/sources.list.d/hashicorp.list
sudo apt update
sudo apt-get install -y terraform
terraform -version

# Ansible - source: https://www.digitalocean.com/community/tutorials/how-to-install-and-configure-ansible-on-ubuntu-20-04
sudo apt-add-repository ppa:ansible/ansible -y
sudo apt update
sudo apt install ansible python3-pip python3-netaddr -y
ansible --version

# SOPS - source: https://github.com/getsops/sops/releases
curl -LO https://github.com/getsops/sops/releases/download/v3.8.1/sops-v3.8.1.linux.amd64
sudo mv sops-v3.8.1.linux.amd64 /usr/local/bin/sops
chmod +x /usr/local/bin/sops
sops --version

# HELM - source: https://helm.sh/docs/intro/install/
curl https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 | bash
helm version

# AZ Cli - source: https://learn.microsoft.com/en-us/cli/azure/install-azure-cli-linux?pivots=apt#option-1-install-with-one-command
curl -sL https://aka.ms/InstallAzureCLIDeb | sudo bash
az --version
```

### Generate an SSH key for Gitlab on your jump host

Now let's generate a certificate for SSH access to our Gitlab repository.

```bash
# Generate ed25519 ssh key
ssh-keygen -t ed25519 -C "jump-host@your-email"

# Copy the output of this command and add it into SSH keys on your Gitlab
# -> How-to guide: https://docs.gitlab.com/ee/user/ssh.html#add-an-ssh-key-to-your-gitlab-account
cat ~/.ssh/id_ed25519.pub
```

### Set up Azure resources needed for this project

Before getting into the project, we need to set up a couple of Azure resources. All resources can be created for free, as a part of the Azure free tier account. If you don't have an Azure account yet, you can use this link to create it: \
<https://azure.microsoft.com/en-us/free>

The first resource we need is the Azure key vault with a key, which will later be used to encrypt files that contain secret values. To do this, you will need an Azure CLI tool. We already installed it on the jump host, so feel free to run all the commands from there but if you prefer to set up all the resources locally, install the CLI tool, using [this](https://learn.microsoft.com/en-us/cli/azure/install-azure-cli) guide from Azure. After that, log into your Azure account, and then create the resource group, key vault, and the key, using the commands below:

---------

BEFORE YOU CONTINUE

When you get to the third command, you may encounter an error, saying something like:
```(Forbidden) Caller is not authorized to perform action on resource.```
You can find a solution to this error in this Stackoverflow post: <https://stackoverflow.com/a/78064558>.
To be able to apply the solution, you need to access the Azure UI here: <https://portal.azure.com>
and log in with your account. After that, you need to search "Key vaults", choose the keyvault that we have created,
which should be called "homelab-dev", and then apply the solution shown in the Stack overflow
link.

---------

```bash
# Log in to you Azure account
az login
# Create a resource group that will hold your resources
az group create --name homelab --location westeurope
#  Create a keyvault
az keyvault create --name homelab-dev --resource-group homelab --location westeurope
# Create a key in the keyvault
az keyvault key create --vault-name homelab-dev --name homelab-dev --protection software
```

The second resource we need is the Azure storage account, and storage container, which will then be used to store the remote backend state file for Terraform. I will be using the same resource group created in the previous step. The storage account name must be unique, which is why we are using the $RANDOM env variable, and storing it in another environment variable, so we can refer to it in the second command. Run the commands below, to create the storage account, and the container:

```bash
# The name has to be unique, which is why we use the $RANDOM variable
STORAGE_ACCOUNT_NAME=tfstate$RANDOM

# Create a storage account
az storage account create --resource-group homelab --name $STORAGE_ACCOUNT_NAME --sku Standard_LRS --encryption-services blob
# Create a container in the storage account
az storage container create --name tfstate-dev --account-name $STORAGE_ACCOUNT_NAME
```

The last thing we need to do is to output and write down all the values that we will need later on in the Terraform and Ansible part of this guide.
I will name the values in the same way as you will encounter them in the guide, so you will know which value belongs where.

#### Azure Variables

```bash
  AZURE-SUBSCRIPTION-ID
  # To retrieve subscription ID, run this command:
  az account show --query id -o tsv
  -----------------
  AZURE-RESOURCE-GROUP
  # We created the resource group, using one of the commands above.
  # Unless you modified the commands, resource group is called "homelab"
  -----------------
  AZURE-STORAGE-ACCOUNT-NAME
  # We created the storage account, using one of the commands above.
  # You can retrieve all the storage account names with this command:
  az storage account list --query "[].name" -o tsv
  -----------------
  AZURE-STORAGE-ACCOUNT-CONTAINER
  # We created the storage account container, using one of the commands above.
  # Unless you modified the commands, storage account container is called "tfstate-dev"
  -----------------
  AZURE-KEYVAULT-KEY
  # We created the Azure Key vault key, using one of the commands above.
  # Unless you modified the commands, you can retrieve the key by running this command:
  az keyvault key show --vault-name homelab-dev --name homelab-dev --query 'key.kid' -o tsv
  # !! PLEASE COPY THE WHOLE URL !!
  -----------------
```

### Buy a domain (OPTIONAL: Only if you want to set up an ingress controller and a cert-manager)

Buy domain - I bought mine on [domain.com](https://domain.com) but feel free to buy it anywhere else. We need a domain because we will then use it to expose services that will be running in our cluster. We will also handle the automatic creation of valid certificates using Certmanager, and ZeroSSL certificate authority. If you don't want to do any of that, and you just want to set up an Nginx ingress controller(or you don't want to have any ingress controller whatsoever, and install it yourself), feel free to also skip the next 2 steps.

### Set up a Cloudflare tunnel (OPTIONAL: Only if you want to expose your cluster without exposing your network)

First, you need to sign up for Cloudflare. It may ask you to Choose your team name and then to fill out payment details but you can skip all this by clicking on the "Cancel and exit" button in the right corner. All we need to do in Cloudflare is to create a Cloudflare tunnel, which is completely free. You can sign up to Cloudflare [here](https://dash.cloudflare.com/sign-up/teams).

After you sign up, you need to add the domain that you purchased to your Cloudflare account. To do that, use a guide linked below. There is a step where it asks you to update nameservers for your domain, if you purchased the domain on [domain.com](https://domain.com) like I did, you can [use this guide](https://www.domain.com/help/article/domain-management-how-to-update-nameservers). If you purchased your domain somewhere else, please refer to the documentation of that provider. You only need to do the first 2 steps - Add site in Cloudflare and Update nameservers. After you update the nameservers, it might take some time before it gets registered by Cloudflare. You will be able to tell it's done when you see the status of your Cloudflare site as Active. You will not be able to create the Cloudflare tunnel without having the site Active, so you might need to wait before proceeding to running the commands below. Here is the [link](https://developers.cloudflare.com/fundamentals/setup/manage-domains/add-site/) to the guide.

Then we can create the Cloudflare tunnel. It is a very simple process, all you need is to download the Cloudflare CLI tool and run 2 commands. This process might be a bit different on Windows machines, so please do it on the jump host machine that we created earlier, so the steps are the same for you. Please fill in the tunnel name(this can be whatever you want), and run the commands below:

```bash
# Install the Cloudflare CLI tool
sudo mkdir -p --mode=0755 /usr/share/keyrings
curl -fsSL https://pkg.cloudflare.com/cloudflare-main.gpg | sudo tee /usr/share/keyrings/cloudflare-main.gpg >/dev/null
echo "deb [signed-by=/usr/share/keyrings/cloudflare-main.gpg] https://pkg.cloudflare.com/cloudflared $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/cloudflared.list
sudo apt-get update && sudo apt-get install cloudflared -y

# Log in to your Cloudflare account
cloudflared tunnel login

# Create the tunnel
cloudflared tunnel create <NAME-OF-THE-TUNNEL> # Name it whatever you want

# Copy the AccountTag, Tunnel ID, Tunnel Secret and write it down somewhere because we will need it in the Ansible part of this guide.
cat ~/.cloudflared/*.json
```

Once you are done with that, you need to do one last thing, which is to create a DNS record that will route trafic coming to your domain and all of its subdomains to the Cloudflare tunnel that you just created. This can be done by two simple commands, you just need to fill in 2 things. The first thing is the name of the tunnel, that you specified previously when you were creating the tunnel. You can also use the Tunnel ID that you got by running the cat command above instead of the name. The second thing is your domain. The first command is to route traffic from every subdomain, and the other one is to route traffic from the root domain. Please fill in the domain, and the tunnel name/id and run the commands below:

```bash
cloudflared tunnel route dns <TUNNEL-NAME-OR-TUNNEL-ID> *.<YOUR-DOMAIN>
cloudflared tunnel route dns <TUNNEL-NAME-OR-TUNNEL-ID> <YOUR-DOMAIN>
```

### Set up a ZeroSSL account (OPTIONAL: Only if you want to have valid certificates when you expose your services)

First, [sign up to ZeroSSL](https://app.zerossl.com/signup).

After you sign up and log in, go to the [Developer tab](https://app.zerossl.com/developer), and click on the "Generate" button in the "EAB Credentials for ACME Clients" box. This will generate and display EAB KID and EAB HMAC Key, write it down somewhere because we will need it in the Ansible part of this guide.

### Set up a Gitlab runner (OPTIONAL: Only if you want to use Gitlab as your CI/CD solution.)

First, you need to [create a Gitlab project](https://docs.gitlab.com/ee/user/project/#create-a-blank-project). After that, open the project, go to Settings -> CI/CD -> Runners, and click the "New project runner" button. Here you only need to fill out the Tags field. The tag field is used for specifying what runner you want to use in a pipeline, you can give it some environment-specific name, like shared-dev, it can be anything. After that, you can click the "Create Runner" button, and you should now see a runner registration guide. The only thing you need to do there is to copy out the runner authentication token that should be present in the first step of the guide. It starts with glrt-. Copy this value, and write it down somewhere because we will need it in the Ansible part of this guide.

### Configure SOPS for secret file encryption

Anytime I need to store secret values in Git, I choose to use a tool called Sops. If you never heard of this tool, here is [a nice article](https://blog.gitguardian.com/a-comprehensive-guide-to-sops) describing how it works.

In short, it's an encryption tool, that can encrypt/decrypt files that contain secret values, using a lot of different encryption methods like GPG, AWS KMS, GCP KMS, or Azure Keyvault.

We have already installed the tool on the jump host but it's a good idea to also install it locally as you will most likely want to add changes to the project from your IDE instead of using Vim/Nano on the jump host machine. To install it on Windows, just download the latest executable from the [Sops GitHub releases](https://github.com/getsops/sops/releases/latest), and [add it to your PATH](https://www.architectryan.com/2018/03/17/add-to-the-path-on-windows-10/). To install it on Linux/MacOS, stick to the [installation guide](https://github.com/getsops/sops/releases/latest).

## STEP 1: Terraform

### [Let's move to the Terraform part of this guide](../terraform/proxmox/kubernetes/k3s/README.md)

## STEP 2: Ansible

### [Let's move to the Ansible part of this guide](../ansible/kubernetes/k3s/README.md)

## STEP 3: Retrieve the Kubecontext, and test the connection to the cluster

### Retrieve kubecontext and create an SSH tunnel to the master node's port 6443 to access the K8s cluster

To interact with a K8s cluster, you first need to make sure, that you have the kubectl tool installed on your local machine. You can find the installation guide for all types of OS [here](https://kubernetes.io/docs/tasks/tools/#kubectl). With that out of the way, let's get into it.

First, log into the jump host machine, and then connect to the K3s master node, using SSH.

```bash
ssh <SSH_USER>@<MASTER-NODE-IP-ADDRESS>
```

Once you are in the master node, run this command, and save the output.

```bash
cat /etc/rancher/k3s/k3s.yaml
```

Exit the master node, and the jump host, and create a ```.kube``` folder in your home folder of your local machine(if you don't have one yet). After you create the ```.kube``` folder, create a file called ```config``` in that folder, and open it in a text editor. Once you open it, paste in the output that we got from the previous command, find the server field, and update it like this:

```bash
# Update this
server: https://<MASTER-NODE-IP-ADDRESS>:6443 # This will be something like https://192.168.0.204:6443

# To this
server: https://127.0.0.1:6443
```

The last step is to create an SSH tunnel, that will create a port forward from port 6443(the K8s API port) on your master node to port 6443 on your local machine, which will allow you to connect to interact with the cluster, using kubectl.

```bash
ssh -L 6443:<MASTER-NODE-IP-ADDRESS>:6443 <JUMP-HOST-USER>@<JUMP_HOST_TAILSCALE_IP>
```

Now try to run a kubectl command to test the connection, and it should work.

```bash
kubectl get ns
```

## Troubleshooting

If you have any issue, please feel free to send me a message on my [Linkedin](https://www.linkedin.com/in/jakub-hovorka/) or at my email <jakub.hovorka.devops@gmail.com>, and I will try to help you.
