# Guides

## [Set up a K3s cluster on Proxmox VMs](./docs/K3S-PROXMOX.md)

In this project, you're going to set up a Proxmox server on a bare metal server, deploy VMs using Terraform, and set up a lightweight K3s Kubernetes cluster, together with multiple useful tools, using Ansible. The tools that are installed on the K8s cluster by Ansible are Ingress Nginx Controller, Cert Manager, ZeroSSL Cluster Issuer, Gitlab runner, and a Cloudflare Tunnel. Mind that installation of any of these tools can be disabled if you don't want to use them, or want to use some alternative. Let's go over these tools a bit, both the Ingress Nginx Controller and a Cert Manager are very popular and standard tools, that are a must-have if you want to expose apps running in your K8s cluster to the public internet.
